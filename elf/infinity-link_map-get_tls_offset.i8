/* Get the offset into the static TLS block of a link map.
   Copyright (C) 2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include "infinity-rtld-private.i8"

/* Given a pointer to a struct link_map, return the offset into
   the static TLS block.  Returns one of NO_TLS_OFFSET or
   FORCED_DYNAMIC_TLS_OFFSET if the link map is not using static
   TLS.  */

define link_map::__get_tls_offset returns ptrdiff_t
	argument link_map_t lm

	add LINK_MAP_L_TLS_OFFSET_OFFSET
	deref ptrdiff_t
	dup
	beq C_NO_TLS_OFFSET, return_NO_TLS_OFFSET
	dup
	beq C_FORCED_DYNAMIC_TLS_OFFSET, return_FORCED_DYNAMIC
	return

return_NO_TLS_OFFSET:
	load NO_TLS_OFFSET
	return

return_FORCED_DYNAMIC:
	load FORCED_DYNAMIC_TLS_OFFSET
	return
