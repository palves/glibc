# -*- coding: utf-8 -*-
# Copyright (C) 2016-17 Free Software Foundation, Inc.
#  This file is part of the GNU C Library.
#
#  The GNU C Library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
#
#  The GNU C Library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with the GNU C Library; if not, see
#  <http://www.gnu.org/licenses/>.  */

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from i8c.runtime import TestCase
import struct

TestCase.import_builtin_constants()
TestCase.import_constants_from("infinity-nptl-constants.h")
TestCase.import_constants_from("infinity-nptl_db-constants.h")

class TestThrGetTLSBase(TestCase):
    TESTFUNC = "thread::get_tlsbase(pi)ip"

    MODID = 2  # TLS module ID
    DTVSI = 56 # dtv_slotinfo for MODID
    DTVGEN = 1 # DTV generation count at dtv[0].counter
    MODGEN = 1 # DTV generation required in dtv_slotinfo

    @TestCase.provide("procservice::getpid()i")
    def __ps_getpid(self):
        self.assertEqual(self.thread, NULL, "should not call")
        return self.MAIN_PID

    @TestCase.provide("libpthread::__lookup_th_unique(i)ip")
    def __libpthread_lookup_th_unique(self, lwpid):
        self.assertEqual(self.thread, NULL, "should not call")
        self.assertEqual(lwpid, self.MAIN_PID)
        return self.LOOKUP_TH_UNIQUE_CODE, self.fd_thread

    @TestCase.provide("dtv_slotinfo::__for_modid(i)p")
    def __dtvsi_for_modid(self, modid):
        self.assertGreater(self.MODID, 0, "should not call")
        self.assertEqual(modid, self.MODID)
        return self.DTVSI

    @TestCase.provide("dtv_slotinfo::__get_link_map(p)p")
    def __dtvsi_get_link_map(self, dtvsi):
        self.assertGreater(self.MODID, 0, "should not call")
        self.assertEqual(dtvsi, self.DTVSI)
        return getattr(self, "LINK_MAP", self.link_map)

    @TestCase.provide("dtv_slotinfo::__get_generation(p)i")
    def __dtvsi_get_generation(self, dtvsi):
        self.assertGreater(self.MODID, 0, "should not call")
        self.assertEqual(dtvsi, self.DTVSI)
        return self.MODGEN

    @TestCase.provide("link_map::__get_tls_offset(p)i")
    def __lm_get_tls_offset(self, lm):
        self.assertTrue(hasattr(self, "tls_offset"), "should not call")
        self.assertEqual(lm, self.link_map.location)
        return self.tls_offset

    def setUp(self):
        # Set up the address space.
        with self.memory.builder() as mem:
            # Create pointers we're not going to dereference
            self.tlsbase = mem.alloc()  # The address we're looking for
            self.link_map = mem.alloc() # Link map, for static TLS
            # Create the DTV
            dtv = mem.alloc()
            if self.MODID > 0:
                dtv.store_uxx(DTV_COUNTER_OFFSET, self.DTVGEN)
                dtvslot = dtv + self.MODID * DTV_T_SIZE
                dtvslot.store_uxx(DTV_POINTER_OFFSET,
                                  getattr(self, "DTVPTR", self.tlsbase))
            # Create the thread
            self.thread = mem.alloc()
            self.thread.store_ptr(PTHREAD_HEADER_DTV_OFFSET, dtv)

    def do_test(self, code=TD_OK):
        result = self.call(self.TESTFUNC, self.thread, self.MODID)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], code)
        if code == TD_OK:
            self.assertEqual(result[1], self.tlsbase.location)

class TestThrGetTLSBase_main_path(TestThrGetTLSBase):
    def test_main_path(self):
        """Test the main path through thread::get_tlsbase"""
        self.do_test()

class TestThrGetTLSBase_bad_modid(TestThrGetTLSBase):
    MODID = 0

    def test_bad_modid(self):
        """Test thread::get_tlsbase with a bad module ID"""
        self.do_test(TD_NOTLS)

class TestThrGetTLSBase_dtv_slotinfo_fail(TestThrGetTLSBase):
    DTVSI = NULL

    def test_no_dtv_slotinfo(self):
        """Test thread::get_tlsbase when dtv_slotinfo::__for_modid fails"""
        self.do_test(TD_ERR)

class TestThrGetTLSBase_bad_link_map(TestThrGetTLSBase):
    LINK_MAP = NULL

    def test_null_link_map(self):
        """Test thread::get_tlsbase when link map is NULL"""
        self.do_test(TD_ERR)

class TestThrGetTLSBase_fake_descriptor(TestThrGetTLSBase):
    MAIN_PID = 12345

    def setUp(self):
        TestThrGetTLSBase.setUp(self)
        self.fd_thread = self.thread
        self.thread = NULL

class TestThrGetTLSBase_FD_ok(TestThrGetTLSBase_fake_descriptor):
    LOOKUP_TH_UNIQUE_CODE = TD_OK

    def warn_caller(self, *args):
        self.warnings.append(args)

    def test_fake_descriptor(self):
        """Test thread::get_tlsbase with a faked descriptor"""
        self.warnings = []
        self.do_test()
        self.assertEqual(self.warnings,
                         [(self.TESTFUNC,
                           "thread descriptor may be uninitialized")])

class TestThrGetTLSBase_FD_fail(TestThrGetTLSBase_fake_descriptor):
    LOOKUP_TH_UNIQUE_CODE = TD_ERR

    def test_fake_descriptor_fail(self):
        """Test thread::get_tlsbase when a faked descriptor lookup fails"""
        self.do_test(TD_TLSDEFER)

class TestThrGetTLSBase_static(TestThrGetTLSBase):
    def setUp(self):
        TestThrGetTLSBase.setUp(self)
        tls_offset = getattr(self, "TLS_OFFSET", None)
        if tls_offset is None:
            if TLS_TCB_AT_TP:
                tls_offset = self.thread.location - self.tlsbase.location
            elif TLS_DTV_AT_TP:
                tls_offset = (self.tlsbase.location
                              - self.thread.location
                              - TLS_PRE_TCB_SIZE)
            else:
                self.fail("TLS_TCB_AT_TP or TLS_DTV_AT_TP must be defined")
        self.tls_offset = tls_offset

class TestThrGetTLSBase_dtv_too_old(TestThrGetTLSBase_static):
    DTVGEN = 0

    def test_dtv_too_old(self):
        """Test thread::get_tlsbase with a too-old DTV"""
        self.do_test()

class TestThrGetTLSBase_marked_static(TestThrGetTLSBase_static):
    DTVPTR = 1

    def test_marked_static(self):
        """Test thread::get_tlsbase with dtv[modid].is_static = 1"""
        self.do_test()

class TestThrGetTLSBase_no_tls(TestThrGetTLSBase_static):
    DTVGEN = 0
    TLS_OFFSET = -1 # NO_TLS_OFFSET from infinity-rtld.i8

    def test_no_tls(self):
        """Test thread::get_tlsbase with NO_TLS_OFFSET"""
        self.do_test(TD_TLSDEFER)

class TestThrGetTLSBase_no_tls(TestThrGetTLSBase_static):
    DTVGEN = 0
    TLS_OFFSET = -2 # FORCED_DYNAMIC_TLS_OFFSET from infinity-rtld.i8

    def test_forced_dynamic(self):
        """Test thread::get_tlsbase with FORCED_DYNAMIC_TLS_OFFSET"""
        self.do_test(TD_TLSDEFER)
