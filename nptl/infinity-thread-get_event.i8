/* Get one element of a thread's event mask.
   Copyright (C) 2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include "infinity-nptl-private.i8"

/* Return one element of a thread's event mask.  */

define thread::get_event returns td_err_e, int
	argument pthread_t descr
	argument int index

	load descr
	beq NULL, fake_descriptor

real_descriptor:
	dup
	blt 0, bad_index
	dup
	bge TD_EVENTSIZE, bad_index
	shr 2

	add
	add PTHREAD_EVENTBUF_EVENTMASK_EVENT_BITS_OFFSET
	deref uint32_t
	load TD_OK
	return

fake_descriptor:
	load 0
	load TD_OK
	return

bad_index:
	load TD_NOAPLIC
	return
